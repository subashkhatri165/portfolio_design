# Subash Khatri
## Project Portfolio
## About 
This is **My Portfolio** website developed with ***HTML and CSS and Javascript.*** You can get information about me from this website. It contains information about myself,programming skills that I have and the projects that I have done.
## Technologies Used
To build this project ***HTML, CSS and Javascript*** were used. To code, I have used Visual Studio Code as text-editor. To baack every single changes of my file I have used VCS which is BitBucket, a remote git repository. I used wireframe technology for drawing the layout. We can use various online tools and applications to draw the layout of the application, but I used simple paper and pencil to draw the layout for this project. HTML5 has been used to build the layout and structure of the website. CSS has been used to give style and make the website more attractive. JavaScript has been used to make the website interactive. Javascript is used here for form ***validation***.

## Description
This Website is describing about me and my experience. There sections in this website like **Header, navbar, about, projects, contact and footer**. Header contains a logo(only my name for now) and navbar. Navbar includes menu containing about ,projects done and contact linking to corresponding content in the website. About me section contains information about me like my introduction,email and also contains my photo.Project Section contains projects done by me. Anyone visiting this website can contact me by sending message through contact me section. And lastly, Footer consist of ***copyright declaration*** and social site links that directly renders my profile of each socail sites. I have also used some images in this website.Images used are my photo, projects photo and social media's icons which can be found in img folder. And also the website is repsponsive with the screen resolution of max 600 and max 1000. The default resolution depends upon the setting of the device you view it.

## Contents of this Project
* Header Section
    * logo or brand name
* Navbar Section

* About Section
    * adding a image
    * details
    
* Project Section
* Contact Section 
* Footer Section

## Header Section
### Tags 
* div with main-container class and header id.
* h1 
    
## Navigation section
* ul
* li
* div tag with navbar and fixed-navbar class

## About Section
* div
* aside
* img
* section
* strong
* h3
* p
* br
* a
## Project Section
* div
* img
* figcaption
* p
* a
## contact Section
* div
* form
    * br
    * input
    * textarea
    * a
 * iframe
 * style

 ## Footer
  * div
    * p
    * ul
    * li
    * a



# Structure Of CSS 

* Body
* main-container
* h1, h2, h3
* header
* section-btn
* section-contact-btn
* main-logo
* nabvar 
* li a
* aside
* section-project
* mapo-container
* footer
* socail Icons
* media query
    *max width of 600
    *max width of 1000


### Main attribute used 
* Flex

### JavaScript
* used js for form validation in name and email field
* i have used a condition on name with less than 3 words
* in email i have given a conditon that the email should have **"@"** and **"."**.

### Responsive
used media query of (max-width 700) and (max-width 1000) to make the portfolio responsive.
